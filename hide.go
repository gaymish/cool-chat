package main

import "os"

const HIDE_SIZE = 10000

var hidden_str string

func hide_init() {
    buf := make([]byte, HIDE_SIZE)

    f, err := os.Open(os.ExpandEnv("$HOME/.coolhide"))
    if err != nil {
        return
    }

    _, err = f.Read(buf)
    if err != nil {
        panic(err)
    }

    hidden_str = string(buf)
}
