package main

import "os"
import "fmt"
import "net"
import "encoding/json"

func main() {
    ui := NewChatUI()
    go ui.Start()

    var me string = os.Args[1]
    var target string = os.Args[2]

    hide_init()

    conn, err := net.Dial("tcp", fmt.Sprintf("%s:1234", target))
    if err != nil {
        println("error connecting:", err)
        os.Exit(1)
    }

    dec := json.NewDecoder(conn)
    enc := json.NewEncoder(conn)

    var other string
    err = dec.Decode(&other)
    if err != nil {
        ui.Inbound <- fmt.Sprintf("Connection Died: %v", err)
        return
    }

    err = enc.Encode(me)
    if err != nil {
        ui.Inbound <- fmt.Sprintf("Connection Died: %v", err)
        return
    }


    ui.SetOther(other)
    go func() {

        for {
            var message string
            err := dec.Decode(&message)
            if err != nil {
                ui.Inbound <- fmt.Sprintf("Connection Died: %v", err)
                return
            }
            ui.Inbound <- message
        }
    }()

    for message := range ui.Outbound {
        err := enc.Encode(message)
        if err != nil {
            ui.Inbound <- fmt.Sprintf("Connection Died: %v", err)
            return
        }
    }
}
